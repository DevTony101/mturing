import p5 from "p5";
import Tape from "./backend/tape";
import Head from "./backend/head";
import getTransitionRules from "./backend/implementation";
import {
  btnCargar,
  hVelocity,
  slVelocity,
  hPasos,
  hEstado,
  hStatus,
} from "./backend/domElements";

let initialString, tape, head;
let newTape = false;
const sketch = (s) => {
  s.setup = () => {
    const canvas = s.createCanvas(1050, 100);
    canvas.parent("parentCanvas");
    s.textAlign(s.CENTER, s.CENTER);
    s.textSize(18);
    tape = new Tape(s, "");
    head = new Head(s, getTransitionRules(), "q0", ["q2"]);
  };

  s.draw = () => {
    s.background(255);
    s.frameRate(parseInt(slVelocity.value));
    tape.display();
    head.display();
    hVelocity.innerHTML = `Velocidad: ${slVelocity.value}`;
    if (newTape) {
      tape = new Tape(s, initialString);
      head.reset();
      head.setTape(tape);
      newTape = false;
    }
    if (head.tape && !head.isDone) {
      head.operate();
      hPasos.innerHTML = `Numero de Pasos: ${head.steps}`;
      hEstado.innerHTML = `Estado Actual: ${head.currentState}`;
    }
  };
};

const sketchInstance = new p5(sketch, "aaa");
function loadString() {
  initialString = document.querySelector("#itEntrada").value;
  btnCargar.setAttribute("disabled", true);
  hStatus.innerHTML = "";
  hStatus.classList.remove("accepted", "rejected");
  newTape = true;
}

btnCargar.addEventListener("click", loadString);
