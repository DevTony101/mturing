import { Directions } from "./transitions/transition";
import TransitionBuilder from "./transitions/transitionBuilder";

export default function getTransitionRules() {
  const rules = [];
  let transition;
  transition = new TransitionBuilder("q0", "a")
    .toState("q0")
    .withSymbol("a")
    .toMove(Directions["RIGHT"])
    .build();
  rules.push(transition);
  transition = new TransitionBuilder("q0", "b")
    .toState("q0")
    .withSymbol("a")
    .toMove(Directions["RIGHT"])
    .build();
  rules.push(transition);
  transition = new TransitionBuilder("q0", " ")
    .toState("q1")
    .withSymbol(" ")
    .toMove(Directions["LEFT"])
    .build();
  rules.push(transition);
  transition = new TransitionBuilder("q1", "a")
    .toState("q1")
    .withSymbol("a")
    .toMove(Directions["LEFT"])
    .build();
  rules.push(transition);
  transition = new TransitionBuilder("q1", " ")
    .toState("q2")
    .withSymbol(" ")
    .toMove(Directions["RIGHT"])
    .build();
  rules.push(transition);
  return rules;
}
