class Transition {
  constructor(qi, x, qj, y, direction) {
    this.qi = qi;
    this.x = x;
    this.qj = qj;
    this.y = y;
    this.direction = direction;
  }
}

const Directions = {
  LEFT: 1,
  RIGHT: -1,
};

export { Transition, Directions };
