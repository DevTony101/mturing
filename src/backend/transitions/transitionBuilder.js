import { Transition } from "./transition";

export default class TransitionBuilder {
  constructor(initialState, readSymbol) {
    this._initialState = initialState;
    this._readSymbol = readSymbol;
  }

  toState(state) {
    this._toState = state;
    return this;
  }

  withSymbol(writeSymbol) {
    this._writeSymbol = writeSymbol;
    return this;
  }

  toMove(direction) {
    this._direction = direction;
    return this;
  }

  build() {
    if (this._toState && this._writeSymbol && this._direction) {
      return new Transition(
        this._initialState,
        this._readSymbol,
        this._toState,
        this._writeSymbol,
        this._direction
      );
    } else {
      throw new Error("Not enough arguments");
    }
  }
}
