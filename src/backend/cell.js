export default class Cell {
  constructor(s, x, y, size, symbol) {
    this.sketch = s;
    this.location = s.createVector(x, y);
    this.size = size;
    this.symbol = symbol;
  }

  show() {
    const { x, y } = this.location;
    this.sketch.push();
    this.sketch.fill(66, 139, 202);
    this.sketch.stroke(255);
    this.sketch.strokeWeight(2);
    this.sketch.translate(x * this.size, y);
    this.sketch.square(0, 0, this.size);
    this.sketch.fill(255);
    this.sketch.strokeWeight(1);
    this.sketch.text(this.symbol + "", this.size * 0.5, this.size * 0.5);
    this.sketch.pop();
  }

  moveX(x) {
    this.location.add(x, 0);
  }
}
