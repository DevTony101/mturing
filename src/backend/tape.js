import Cell from "./cell";

export default class Tape {
  constructor(s, string) {
    this.sketch = s;
    this.tape = [];
    this.canMove = false;
    this.middleCell = 110;
    this.string = string;
    this.init();
  }

  init() {
    for (let i = -100; i < 100; i++) {
      if (this.string && i >= 9 && String(this.string).length + 10 >= i) {
        if (i === 9 || i === String(this.string).length + 10) {
          this.tape.push(new Cell(this.sketch, i, 0, 50, " "));
        } else {
          this.tape.push(new Cell(this.sketch, i, 0, 50, this.string[i - 10]));
        }
      } else {
        this.tape.push(new Cell(this.sketch, i, 0, 50, " "));
      }
    }
  }

  getCurrentCell() {
    return this.tape[this.middleCell];
  }

  moveTape(units) {
    this.tape.forEach((el) => el.moveX(units));
  }

  display() {
    this.tape.forEach((el) => el.show());
  }
}
