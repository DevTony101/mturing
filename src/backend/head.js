import { btnCargar, hStatus } from "./domElements";

export default class Head {
  constructor(s, transitionRules, initialState, acceptedStates) {
    this.sketch = s;
    this.rotations = 1;
    this.rules = [...transitionRules];
    this.initialState = initialState;
    this.acceptedStates = [...acceptedStates];
    console.log(this.acceptedStates);
    this.currentState = initialState;
    this.isDone = false;
    this.tape = undefined;
    this.steps = 0;
  }

  setTape(tape) {
    this.tape = tape;
  }

  reset() {
    this.steps = 0;
    this.isDone = false;
    this.tape = undefined;
    this.currentState = this.initialState;
  }

  display() {
    const wh = this.sketch.width;
    const p1 = this.sketch.createVector(wh / 2 - 15, 70);
    const p2 = this.sketch.createVector(wh / 2, 40);
    const p3 = this.sketch.createVector(wh / 2 + 15, 70);
    this.sketch.push();
    this.sketch.fill(0);
    this.sketch.stroke(255);
    this.sketch.strokeWeight(2);
    this.sketch.triangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
    this.sketch.pop();
  }

  operate() {
    if (!this.tape.canMove) {
      const currentCell = this.tape.getCurrentCell();
      const transRule = this.rules.find((rule) => {
        const a = rule.qi === this.currentState;
        const b = rule.x === currentCell.symbol;
        return a && b;
      });
      if (transRule) {
        this.currentState = transRule.qj;
        currentCell.symbol = transRule.y;
        this.direction = transRule.direction;
        this.tape.canMove = true;
      } else {
        this.isDone = true;
        btnCargar.removeAttribute("disabled");
        if (this.acceptedStates.includes(this.currentState)) {
          hStatus.innerHTML = "Aceptado!";
          hStatus.classList.add("accepted");
        } else {
          hStatus.innerHTML = "Rechazado!";
          hStatus.classList.add("rejected");
        }
      }
    } else {
      this.moveTape();
    }
  }

  moveTape() {
    if (this.rotations < 10) {
      this.tape.moveTape(this.direction * 0.1);
      this.rotations++;
      if (this.rotations >= 10) {
        this.steps++;
        this.rotations = 1;
        this.tape.canMove = false;
        this.tape.middleCell += this.direction * -1;
      }
    }
  }
}
