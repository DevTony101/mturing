const slVelocity = document.querySelector("#slVelocity");
const hVelocity = document.querySelector("#hVelocity");
const btnCargar = document.querySelector("#btnCargar");
const hPasos = document.querySelector("#hPasos");
const hEstado = document.querySelector("#hEstado");
const hStatus = document.querySelector("#hStatus");

export { btnCargar, hVelocity, slVelocity, hPasos, hEstado, hStatus };
